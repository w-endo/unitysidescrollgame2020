﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    float speed = 0;
    Vector2 startPos;

    // Start is called before the first frame update
    void Start()
    { 
    
        
    }

    // Update is called once per frame
    void Update()
    {


        if (Input.GetMouseButtonDown(0))
        {
            startPos = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            Vector2 endPos = Input.mousePosition;

            float swipeLength = endPos.x - startPos.x;

            speed = swipeLength / 500;

            GetComponent<AudioSource>().Play();
        }

        this.transform.Translate(speed, 0.0f, 0.0f);

        //ちょっとずつ減速
        speed = speed * 0.98f;  
    }
}
